# SIDLOC Documentation #

SIDLOC Specifications and Documentation

## Documentation ##

- System Requirements Specification
  - [Unstable](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/master/raw/build/requirements-specification.pdf?job=docs_devel)
  - Stable
    - [1.3.0](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/1.3.0/raw/build/requirements-specification.pdf?job=docs_release)
    - [1.2.0](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/1.2.0/raw/build/requirements-specification.pdf?job=docs_release)
    - [1.1.0](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/1.1.0/raw/build/requirements-specification.pdf?job=docs_release)
    - [1.0.0](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/1.0.0/raw/build/requirements-specification.pdf?job=docs_release)
- System Design Document
  - [Unstable](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/master/raw/build/design-document.pdf?job=docs_devel)
  - Stable
    - [1.3.0](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/1.3.0/raw/build/design-document.pdf?job=docs_release)
- Interface Control Document
  - [Unstable](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/master/raw/build/interface-control-document.pdf?job=docs_devel)
  - Stable
    - [1.3.0](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/1.3.0/raw/build/interface-control-document.pdf?job=docs_release)
- Verification Plan
  - [Unstable](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/master/raw/build/verification-plan.pdf?job=docs_devel)
  - Stable
    - [1.3.0](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/1.3.0/raw/build/verification-plan.pdf?job=docs_release)
    - [1.2.0](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/1.2.0/raw/build/verification-plan.pdf?job=docs_release)
    - [1.1.0](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/1.1.0/raw/build/verification-plan.pdf?job=docs_release)
- Experiments
  - Ariane 6
    - [Unstable](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/master/raw/build/experiment-ariane-6.pdf?job=docs_devel)
    - Stable
      - [1.3.0](https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs/artifacts/1.3.0/raw/build/experiment-ariane-6.pdf?job=docs_release)

## Rendering ##

### Requirements ###

- TeX Live (full) >= 2022
- CMake >= 2.8
- Python >= 3.6
- python-gitlab >= 3.6.0
- pandoc >= 2.17.1.1
- pypandoc >= 1.8
- Jinja2 >= 3.0.0
- chktex >= 1.7.6

### Checking-out source ###

To check-out the source code:

```
git clone https://git.overleaf.com/60be9d12dcf6a9d028e041b1 sidloc-docs
cd sidloc-docs
```

### Building ###

To render the documents in PDF format:

```
mkdir build
cd build
cmake ..
make pdf
```

### Linting ###

To lint the source code of the documents:

```
chktex *.tex
```

## License ##

[![license](https://img.shields.io/badge/license-CC%20BY--SA%204.0-6672D8.svg)](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202021-Libre%20Space%20Foundation-6672D8.svg)](https://libre.space/)
