#!/bin/sh -e
#
# Script to generate Git version and date as LaTeX commands
#
# Copyright (C) 2022 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

if [ -z "$1" ]; then
	exit 1
fi
commit_date=$(git log -1 --format=format:%cs)
version=$(git describe)

cat <<EOF >"$1"
\newcommand{\gitver}{$version}
\newcommand{\gitdate}{$commit_date}
EOF
