#!/bin/sh -e
#
# Script to download GitLab expanded TeX document artifacts
#
# Copyright (C) 2025 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

if [ -z "$1" ]; then
	exit 1
fi

git_url="${CI_REPOSITORY_URL:-https://gitlab.com/librespacefoundation/sidloc/sidloc-docs.git}"
job_base_url="${CI_JOB_URL%/*}"
job_base_url="${job_base_url:-https://gitlab.com/librespacefoundation/sidloc/sidloc-docs/-/jobs}"
git_prev_tag="$(git ls-remote -q --tags --refs --exit-code --sort="v:refname" "$git_url" | awk 'BEGIN { FS="/" } END { print $3 }')"

curl -fsLo "${1}.old" "${job_base_url}/artifacts/${git_prev_tag}/raw/build/$1?job=docs_release"
