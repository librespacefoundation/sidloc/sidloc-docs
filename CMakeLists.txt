cmake_minimum_required(VERSION 3.5)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/CMakeModules")

project(documentation NONE)

include(UseLATEX)

add_custom_command(
  OUTPUT
  include/requirements.tex
  COMMAND python3 ${CMAKE_SOURCE_DIR}/contrib/issue-template.py --project "librespacefoundation/sidloc/sidloc-requirements" --label "Requirement" --output-file include/requirements.tex "${CMAKE_SOURCE_DIR}/templates/include/requirements.tex.j2"
)

add_custom_command(
  OUTPUT
  include/test-cases.tex
  COMMAND python3 ${CMAKE_SOURCE_DIR}/contrib/issue-template.py --project "librespacefoundation/sidloc/sidloc-verification" --label "Test Case" --output-file include/test-cases.tex "${CMAKE_SOURCE_DIR}/templates/include/test-cases.tex.j2"
)

add_custom_command(
  OUTPUT
  include/use-cases.tex
  COMMAND python3 ${CMAKE_SOURCE_DIR}/contrib/issue-template.py --project "librespacefoundation/sidloc/sidloc-requirements" --label "Use Case" --output-file include/use-cases.tex "${CMAKE_SOURCE_DIR}/templates/include/use-cases.tex.j2"
)

add_custom_command(
  OUTPUT
  include/gitver.tex
  COMMAND sh ${CMAKE_SOURCE_DIR}/contrib/gitver.sh include/gitver.tex
)

add_latex_document(requirements-specification.tex
  INPUTS
  glossaries/acronyms.tex
  include/version.tex
  DEPENDS
  include/requirements.tex
  include/gitver.tex
  USE_GLOSSARY
  BIBFILES references.bib
  IMAGE_DIRS images
)

add_custom_command(
  OUTPUT
  requirements-specification.expanded.tex
  DEPENDS
  requirements-specification
  COMMAND latexpand requirements-specification.tex --empty-comments --keep-comments -o requirements-specification.expanded.tex
)

add_custom_command(
  OUTPUT
  requirements-specification.expanded.tex.old
  COMMAND sh ${CMAKE_SOURCE_DIR}/contrib/getartifact.sh requirements-specification.expanded.tex
)

add_custom_command(
  OUTPUT
  requirements-specification.expanded.pre.tex
  requirements-specification.expanded.pre.tex.old
  DEPENDS
  requirements-specification.expanded.tex
  requirements-specification.expanded.tex.old
  COMMAND grep -v "\\(\\IfFileExists{.\\+}{%\\|}{} % chktex 27\\)$" requirements-specification.expanded.tex > requirements-specification.expanded.pre.tex
  COMMAND grep -v "\\(\\IfFileExists{.\\+}{%\\|}{} % chktex 27\\)$" requirements-specification.expanded.tex.old > requirements-specification.expanded.pre.tex.old
  VERBATIM
)

add_custom_command(
  OUTPUT
  requirements-specification.diff.tex
  DEPENDS
  requirements-specification.expanded.pre.tex
  requirements-specification.expanded.pre.tex.old
  COMMAND latexdiff --append-textcmd="newcommand" requirements-specification.expanded.pre.tex.old requirements-specification.expanded.pre.tex > requirements-specification.diff.tex
)

add_latex_document(requirements-specification.diff.tex
  DEPENDS
  include/requirements.tex
  include/gitver.tex
  requirements-specification.diff.tex
  USE_GLOSSARY
  BIBFILES references.bib
  IMAGE_DIRS images
  EXCLUDE_FROM_ALL
  EXCLUDE_FROM_DEFAULTS
)

add_latex_document(design-document.tex
  INPUTS
  glossaries/acronyms.tex
  include/version.tex
  include/requirements-urls.tex
  DEPENDS
  include/requirements.tex
  include/gitver.tex
  USE_GLOSSARY
  BIBFILES references.bib
  IMAGE_DIRS images
)

add_custom_command(
  OUTPUT
  design-document.expanded.tex
  DEPENDS
  design-document
  COMMAND latexpand design-document.tex --empty-comments --keep-comments -o design-document.expanded.tex
)

add_custom_command(
  OUTPUT
  design-document.expanded.tex.old
  COMMAND sh ${CMAKE_SOURCE_DIR}/contrib/getartifact.sh design-document.expanded.tex
)

add_custom_command(
  OUTPUT
  design-document.expanded.pre.tex
  design-document.expanded.pre.tex.old
  DEPENDS
  design-document.expanded.tex
  design-document.expanded.tex.old
  COMMAND grep -v "\\(\\IfFileExists{.\\+}{%\\|}{} % chktex 27\\)$" design-document.expanded.tex > design-document.expanded.pre.tex
  COMMAND grep -v "\\(\\IfFileExists{.\\+}{%\\|}{} % chktex 27\\)$" design-document.expanded.tex.old > design-document.expanded.pre.tex.old
  VERBATIM
)

add_custom_command(
  OUTPUT
  design-document.diff.tex
  DEPENDS
  design-document.expanded.pre.tex
  design-document.expanded.pre.tex.old
  COMMAND latexdiff --append-textcmd="newcommand" design-document.expanded.pre.tex.old design-document.expanded.pre.tex > design-document.diff.tex
)

add_latex_document(design-document.diff.tex
  DEPENDS
  include/requirements.tex
  include/gitver.tex
  design-document.diff.tex
  USE_GLOSSARY
  BIBFILES references.bib
  IMAGE_DIRS images
  EXCLUDE_FROM_ALL
  EXCLUDE_FROM_DEFAULTS
)

add_latex_document(interface-control-document.tex
  INPUTS
  glossaries/acronyms.tex
  include/version.tex
  DEPENDS
  include/gitver.tex
  USE_GLOSSARY
)

add_custom_command(
  OUTPUT
  interface-control-document.expanded.tex
  DEPENDS
  interface-control-document
  COMMAND latexpand interface-control-document.tex --empty-comments --keep-comments -o interface-control-document.expanded.tex
)

add_custom_command(
  OUTPUT
  interface-control-document.expanded.tex.old
  COMMAND sh ${CMAKE_SOURCE_DIR}/contrib/getartifact.sh interface-control-document.expanded.tex
)

add_custom_command(
  OUTPUT
  interface-control-document.expanded.pre.tex
  interface-control-document.expanded.pre.tex.old
  DEPENDS
  interface-control-document.expanded.tex
  interface-control-document.expanded.tex.old
  COMMAND grep -v "\\(\\IfFileExists{.\\+}{%\\|}{} % chktex 27\\)$" interface-control-document.expanded.tex > interface-control-document.expanded.pre.tex
  COMMAND grep -v "\\(\\IfFileExists{.\\+}{%\\|}{} % chktex 27\\)$" interface-control-document.expanded.tex.old > interface-control-document.expanded.pre.tex.old
  VERBATIM
)

add_custom_command(
  OUTPUT
  interface-control-document.diff.tex
  DEPENDS
  interface-control-document.expanded.pre.tex
  interface-control-document.expanded.pre.tex.old
  COMMAND latexdiff --append-textcmd="newcommand" interface-control-document.expanded.pre.tex.old interface-control-document.expanded.pre.tex > interface-control-document.diff.tex
)

add_latex_document(interface-control-document.diff.tex
  DEPENDS
  include/gitver.tex
  interface-control-document.diff.tex
  USE_GLOSSARY
  EXCLUDE_FROM_ALL
  EXCLUDE_FROM_DEFAULTS
)

add_latex_document(verification-plan.tex
  INPUTS
  glossaries/acronyms.tex
  include/version.tex
  DEPENDS
  include/use-cases.tex
  include/test-cases.tex
  include/gitver.tex
  USE_GLOSSARY
  IMAGE_DIRS images
)

add_custom_command(
  OUTPUT
  verification-plan.expanded.tex
  DEPENDS
  verification-plan
  COMMAND latexpand verification-plan.tex --empty-comments --keep-comments -o verification-plan.expanded.tex
)

add_custom_command(
  OUTPUT
  verification-plan.expanded.tex.old
  COMMAND sh ${CMAKE_SOURCE_DIR}/contrib/getartifact.sh verification-plan.expanded.tex
)

add_custom_command(
  OUTPUT
  verification-plan.expanded.pre.tex
  verification-plan.expanded.pre.tex.old
  DEPENDS
  verification-plan.expanded.tex
  verification-plan.expanded.tex.old
  COMMAND grep -v "\\(\\IfFileExists{.\\+}{%\\|}{} % chktex 27\\)$" verification-plan.expanded.tex > verification-plan.expanded.pre.tex
  COMMAND grep -v "\\(\\IfFileExists{.\\+}{%\\|}{} % chktex 27\\)$" verification-plan.expanded.tex.old > verification-plan.expanded.pre.tex.old
  VERBATIM
)

add_custom_command(
  OUTPUT
  verification-plan.diff.tex
  DEPENDS
  verification-plan.expanded.pre.tex
  verification-plan.expanded.pre.tex.old
  COMMAND latexdiff --append-textcmd="newcommand" verification-plan.expanded.pre.tex.old verification-plan.expanded.pre.tex > verification-plan.diff.tex
)

add_latex_document(verification-plan.diff.tex
  DEPENDS
  include/use-cases.tex
  include/test-cases.tex
  include/gitver.tex
  verification-plan.diff.tex
  USE_GLOSSARY
  IMAGE_DIRS images
  EXCLUDE_FROM_ALL
  EXCLUDE_FROM_DEFAULTS
)

add_latex_document(experiment-ariane-6.tex
  INPUTS
  glossaries/acronyms.tex
  include/version.tex
  include/requirements-urls.tex
  DEPENDS
  include/requirements.tex
  include/gitver.tex
  USE_GLOSSARY
  BIBFILES references.bib
  IMAGE_DIRS images
)

add_custom_command(
  OUTPUT
  experiment-ariane-6.expanded.tex
  DEPENDS
  experiment-ariane-6
  COMMAND latexpand experiment-ariane-6.tex --empty-comments --keep-comments -o experiment-ariane-6.expanded.tex
)

add_custom_command(
  OUTPUT
  experiment-ariane-6.expanded.tex.old
  COMMAND sh ${CMAKE_SOURCE_DIR}/contrib/getartifact.sh experiment-ariane-6.expanded.tex
)

add_custom_command(
  OUTPUT
  experiment-ariane-6.expanded.pre.tex
  experiment-ariane-6.expanded.pre.tex.old
  DEPENDS
  experiment-ariane-6.expanded.tex
  experiment-ariane-6.expanded.tex.old
  COMMAND grep -v "\\(\\IfFileExists{.\\+}{%\\|}{} % chktex 27\\)$" experiment-ariane-6.expanded.tex > experiment-ariane-6.expanded.pre.tex
  COMMAND grep -v "\\(\\IfFileExists{.\\+}{%\\|}{} % chktex 27\\)$" experiment-ariane-6.expanded.tex.old > experiment-ariane-6.expanded.pre.tex.old
  VERBATIM
)

add_custom_command(
  OUTPUT
  experiment-ariane-6.diff.tex
  DEPENDS
  experiment-ariane-6.expanded.pre.tex
  experiment-ariane-6.expanded.pre.tex.old
  COMMAND latexdiff --append-textcmd="newcommand" experiment-ariane-6.expanded.pre.tex.old experiment-ariane-6.expanded.pre.tex > experiment-ariane-6.diff.tex
)

add_latex_document(experiment-ariane-6.diff.tex
  DEPENDS
  include/requirements.tex
  include/gitver.tex
  experiment-ariane-6.diff.tex
  USE_GLOSSARY
  BIBFILES references.bib
  IMAGE_DIRS images
  EXCLUDE_FROM_ALL
  EXCLUDE_FROM_DEFAULTS
)

add_custom_target(
  expand
  DEPENDS
  requirements-specification.expanded.tex
  design-document.expanded.tex
  verification-plan.expanded.tex
  interface-control-document.expanded.tex
  experiment-ariane-6.expanded.tex
)

add_custom_target(
  diff
  DEPENDS
  requirements-specification.diff
  design-document.diff
  verification-plan.diff
  interface-control-document.diff
  experiment-ariane-6.diff
)
