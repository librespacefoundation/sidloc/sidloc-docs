%% SIDLOC - Verification Plan
%% Copyright (C) 2022 Libre Space Foundation
%%
%% This work is licensed under a
%% Creative Commons Attribution-ShareAlike 4.0 International License.
%%
%% You should have received a copy of the license along with this
%% work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

\documentclass[english,title,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{float}
\usepackage[hidelinks]{hyperref}
\usepackage[toc,acronyms,section,nopostdot,nonumberlist,section=section,numberedsection]{glossaries}
\usepackage[toc]{appendix}
\usepackage{graphicx}
\usepackage{lastpage}
\usepackage{titlesec}
\usepackage{fancyhdr}
\usepackage{lipsum}
\usepackage{booktabs}
\usepackage[maxfloats=500]{morefloats}
\usepackage{amssymb}
\usepackage{textcomp,gensymb}
\usepackage{pifont}
\usepackage{verbatim}
\usepackage{rotating}
\usepackage{makecell}
\usepackage{longtable}
\usepackage{rotating}

\maxdeadcycles=200

\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

\fancypagestyle{plain}{
  \fancyhf{}
  \fancyhead[L]{SIDLOC}
  \fancyhead[R]{Verification Plan}
  \fancyfoot[L]{Version \version}
  \fancyfoot[C]{\thepage{} of~\pageref{LastPage}}
  \fancyfoot[R]{\copyright{} Libre Space Foundation}
  \renewcommand{\headrulewidth}{0.4pt}
  \renewcommand{\footrulewidth}{0.4pt}
}
\pagestyle{plain}

\input{include/version}

\makeglossaries{}

%% Acronyms
\input{glossaries/acronyms}

\title{
  Verification Plan \\
  \large{SIDLOC}
}
\author{Libre Space Foundation}
\date{\today\\Version \version}

\titleformat{\chapter}[hang]
{\normalfont\bfseries\Huge}{\thechapter.}{10pt}{}

\begin{document}

\hypersetup{pageanchor=false}
\maketitle
\hypersetup{pageanchor=true}
\tableofcontents

\chapter{Changelog}

\begin{tabular}{llp{7cm}}
  \toprule
  Version & Date             & Changes                            \\
  \midrule
  1.3.0   & March 8, 2023    & \textbullet{} Improved syntax \\
  \midrule
  1.2.0   & March 9, 2022    & \textbullet{} TC-8 Updated with more detailed steps (ERR RID BPO-05) \newline
                               \textbullet{} TC-13 Clarified access interface and medium (ERR RID BPO-09) \newline
                               \textbullet{} TC-20 Replaced ECSS with NASA standard, update sequence to mention test in temperature extremes (ERR RID BPO-11) \newline
                               \textbullet{} TC-11 Updated with more detailed steps (ERR RID BPO-08) \newline
                               \textbullet{} TC-21 Added reference to NASA GEVS standard for loads and frequencies (ERR RID BPO-12) \newline
                               \textbullet{} TC-22 Added reference to NASA GEVS standard for loads and frequencies (ERR RID BPO-12) \newline
                               \textbullet{} TC-23 Added reference to NASA GEVS standard for loads and frequencies (ERR RID BPO-12) \newline
                               \textbullet{} TC-24 Added reference to NASA GEVS standard for loads and frequencies (ERR RID BPO-12) \newline
                               \textbullet{} TC-27 Removed futile sequence step (ERR RID BPO-14) \newline
                               \textbullet{} TC-28 Rephrased to reflect dimensional accuracy check (ERR RID BPO-15) \newline
                               \textbullet{} TC-55 Fixed typo \newline
                               \textbullet{} TC-67 Fixed operational temperatures to comply with LSP-REQ-317.01 (ERR RID BPO-01) \newline
          \\
  \midrule
  1.1.0   & February 9, 2022 & \textbullet{} First stable release \\
          \\
  \bottomrule
\end{tabular}

\chapter{Introduction}

\section{Purpose and Scope}

This document describes verification and test procedures which verify that the implementation of `SIDLOC' complies with the defined System Requirement Specification.
It includes a verification plan, test plan and description of tools, \acrshort{gse} and facilities.

\printglossary[type=\acronymtype]

\chapter{Product Presentation}

SIDLOC is a satellite tracking and identification system which can detect, catalogue and predict the positions of space objects.
The goal of this system is to support and promote \acrshort{ssa} and \acrshort{stm}, which are expected to play a key role in the near future with the ever-expanding demand for satellites and their miniaturization.
The system can provide a cost-effective solution for risk management while in orbit, during post-commissioning or re-entry.
It can also serve as an independent source of spacecraft identification and tracking information that can be used to enhance the existing ones or as a backup solution.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.65\textwidth]{images/sidloc-functions.pdf}
  \caption{SIDLOC components}\label{fig:system-components}
\end{figure}

\chapter{Verification Plan}

\section{Verification approach}

\subsection{Verification Methods}

Verification is accomplished with at least one of the methods below depending on schedule and cost constraints:
\begin{itemize}
\item Inspection:\\
  Verification by inspection consists of the visual determination of physical characteristics, for example, constructional features, hardware conformance to document drawing or workmanship requirements, physical conditions, software source code conformance with coding standards etc.
\item Review of design:\\
  Verification by \acrshort{rod} consists of using approved records or evidence that unambiguously shows that the requirement is met.
\item Analysis:\\
  Verification by analysis consists of performing theoretical or empirical evaluation using techniques such as systematic, statistical and
  qualitative design analysis, modelling and computational simulation etc.
\item Test:\\
  Verification by test consists of measuring product performance and functions under representative simulated environments.
\end{itemize}

\subsection{Verification Levels}

Requirements are verified in three different verification levels:

\begin{itemize}
\item System Level: The SIDLOC end-to-end system.
\item Segment level: The ground and space segments.
\item Equipment/Unit Level: The components of ground and space segments.
\end{itemize}

\section{Model philosophy}

The Model philosophy that is selected for SIDLOC is the hybrid model philosophy.
This approach's objective is reducing risks while maintaining a balance in schedule and cost.

The output of this activity is a \acrfull{fm} for a satellite tracking and identification system. By having a new design with \acrshort{cots} hardware and by following agile development, the models needed for each verification level of SIDLOC are described below (\textit{Note: Some of the models that will be used in different verification levels, may be identical/represented by the same hardware}):

\begin{itemize}
\item System Level
  \begin{itemize}
  \item \acrfull{dm}
  \item \acrfull{efm}
  \item \acrfull{s-tm}
  \item \acrfull{eqm}
  \item Suitcase Model
  \item \acrfull{fm}
  \end{itemize}

\item Segment level
  \begin{itemize}
  \item \acrfull{dm}
  \item \acrfull{efm}
  \item \acrfull{eqm}
  \item Ground Segment Models
    \begin{itemize}
    \item Model for developing and verifying procedures and the \acrfull{hmi}
    \item Model for \acrfull{tm} data processing and associated procedures
    \end{itemize}
  \end{itemize}

\item Equipment/Unit Level
  \begin{itemize}
  \item \acrfull{mu}
  \item \acrfull{dm}

  \end{itemize}

\end{itemize}

\section{Verification strategy}

The verification strategy of SIDLOC is a combination of the different verification methods, giving emphasis on the test method at the different verification levels as described in the previous section.
Following this strategy is the way to verify, test and fulfil requirements defined in System Requirement Specification and the corresponding GitLab requirement issues.

\section{Verification control methodology}

Monitoring and controlling the verification procedures are done through the issue tracker of open source git-repository manager GitLab, by tracking verification issues for each method applied at each level.
GitLab issue tracker offers to organize and manage tools to effectively monitor and control the verification process.

\chapter{Test Plan}

Verification by a test is performed at all three verification levels of the SIDLOC system in order to ensure that System Requirement Specification is satisfied. Test cases and the requirements that are covered can be found in Appendix A.

\section{Roles}

There are three roles that participate in the testing plan with respective responsibilities.

\begin{itemize}
\item Developer: \\
  Responsible for the implementation of technical specifications and low-level test cases.
\item Tester: \\
  Responsible for implementing \acrshort{e2e} tests and testing the technical specification implementation.
\item Test Manager: \\
  Responsible for test plan execution by reviewing test reports and, based on that, accepting each level of technical specification implementation.
\end{itemize}

\section{Levels}

There are three different levels of testing:

\subsubsection{Unit Testing}

The purpose of this level is to test the smallest units of implementation and facilitate the development process.
Unit tests are implemented and executed by the developer.
There is no mapping or traceability of unit tests to system requirements.
Unit tests are executed automatically to generate test coverage reports that are used as feedback for proof of testing, risk assessment and acceptance of implementation by the Test Manager.

\subsubsection{Functional/Module Testing}

The purpose of this level is to verify the correct implementation of low-level functional requirements for each ground and space segment.
Functional tests are implemented by the developer and executed by the tester.
Mapping and traceability of functional tests to system requirements is provided indirectly through the association of low-level functional requirements to high-level system requirements.
Functional tests are executed automatically and generate pass/fail reports that are used as feedback for proof of testing, risk assessment and acceptance of implementation by the Test Manager.

\subsubsection{Integration/End-to-end Testing}

The purpose of this level is to verify the correct implementation of high-level system requirements.
\acrshort{e2e} tests are implemented and executed by the tester.
There are direct mapping and traceability of \acrshort{e2e} tests to system requirements.
\acrshort{e2e} tests are executed automatically and generate pass/fail reports that are used as feedback for proof of testing, risk assessment and acceptance of implementation by the Test Manager.

\chapter{Tools  and test facilities}

In this section are described tools, \acrshort{gse} and test facilities that are available and required to execute the verification.

\subsubsection{Collection of test data}

From previous development efforts and SatNOGS development, there are available random and structured data.

\subsubsection{RF measurement equipment}

The electronics lab of \acrshort{lsf} facilities is equipped with Spectrum analyzers, power meters, attenuators, power supplies and vector network analyzers.
Additionally, \acrshort{emc} testing facilities can be coordinated with EMC Hellas.

\subsubsection{UHF receivers and transmitters}

\acrshort{sdr} devices that meet required specifications are available in \acrshort{lsf} facilities.

\subsubsection{Software simulators/emulators}

\acrshort{cots} and open source simulators and emulators have been used in previous COMMS development efforts by \acrshort{lsf} and are available in \acrshort{lsf} facilities.

\subsubsection{Microcontroller and FPGA equipment}

Programmers and development kits for microcontroller and FPGA exist in \acrshort{lsf} facilities.

\subsubsection{TVAC for environment testing}

\acrshort{lsf} has secured access to a Greek University TVAC facility that meets the verification and test requirements.

\subsubsection{Vibration testing equipment}

\acrshort{lsf} has secured access to the Hellenic Aerospace Industry Inc.\ and to the NanoSat Lab of The Polytechnic University of Catalonia vibrational testing facilities.

\subsubsection{Testing in Anechoic chamber}

\acrshort{lsf} has secured access to the University of West Attica anechoic chamber.

\begin{appendices}

\IfFileExists{include/test-cases}{\input{include/test-cases}}{} % chktex 27

\chapter{Test Cases}

\ifdefined\TestCases{}
\TestCases{}
\fi

\end{appendices}

\end{document}
