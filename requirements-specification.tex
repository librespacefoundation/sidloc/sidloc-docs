%% SIDLOC - System Requirements Specification
%% Copyright (C) 2021, 2022 Libre Space Foundation
%%
%% This work is licensed under a
%% Creative Commons Attribution-ShareAlike 4.0 International License.
%%
%% You should have received a copy of the license along with this
%% work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

\documentclass[english,title,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{float}
\usepackage[hidelinks]{hyperref}
\usepackage[toc,acronyms,section,nopostdot,nonumberlist,section=section,numberedsection]{glossaries}
\usepackage[toc]{appendix}
\usepackage{graphicx}
\usepackage{lastpage}
\usepackage{titlesec}
\usepackage{fancyhdr}
\usepackage{lipsum}
\usepackage{booktabs}
\usepackage[maxfloats=500]{morefloats}

\fancypagestyle{plain}{
  \fancyhf{}
  \fancyhead[L]{SIDLOC}
  \fancyhead[R]{System Requirements Specification}
  \fancyfoot[L]{Version \version}
  \fancyfoot[C]{\thepage{} of~\pageref{LastPage}}
  \fancyfoot[R]{\copyright{} Libre Space Foundation}
  \renewcommand{\headrulewidth}{0.4pt}
  \renewcommand{\footrulewidth}{0.4pt}
}
\pagestyle{plain}

\input{include/version}

\makeglossaries{}

%% Acronyms
\input{glossaries/acronyms}

\title{
  System Requirements Specification \\
  \large{SIDLOC}
}
\author{Libre Space Foundation}
\date{\versiondate\\Version \version}

\titleformat{\chapter}[hang]
{\normalfont\bfseries\Huge}{\thechapter.}{10pt}{}

\begin{document}

\hypersetup{pageanchor=false}
\maketitle
\hypersetup{pageanchor=true}
\tableofcontents

\chapter{Changelog}

\begin{tabular}{llp{6.5cm}}
  \toprule
  Version & Date              & Changes                                             \\
  \midrule
  1.3.0   & March 8, 2023     & \textbullet{} Improved syntax \\
  \midrule
  1.2.0   & March 9, 2022     & \textbullet{} REQ-67 Fixed operational temperatures \\
  \midrule
  1.1.0   & February 9, 2022  & \textbullet{} Added separate sections for Experiment and Specification requirements \newline
                                \textbullet{} REQ-24 Extended rationale \newline
                                \textbullet{} REQ-29 Dropped as out of scope \newline
                                \textbullet{} REQ-50 Replaced PPM with Hz \newline
                                \textbullet{} REQ-53 Replaced SR with altitude \newline
                                \textbullet{} REQ-54 Fixed typo in description \newline
                                \textbullet{} REQ-56 Specified power monitor \newline
                                \textbullet{} REQ-58 Fixed typo in description \newline
                                \textbullet{} REQ-62 Specified methods of software quality assurance \newline
                                \textbullet{} REQ-64 Dropped as superfluous \newline
                                \textbullet{} REQ-65 Update rationale with suggested protocols \newline
                                \textbullet{} REQ-67 Specified temperature ranges \newline
                                \textbullet{} REQ-69 Specified EMI/EMC standards \newline
                                \textbullet{} REQ-70 Specified ECC memory as SEE mitigation \newline
                                \textbullet{} REQ-79 Fixed typo in description \newline
                                \textbullet{} REQ-87 Specified data formats \newline
                                \textbullet{} REQ-93 Specified power sources \newline
          \\
  \midrule
  1.0.0   & November 22, 2021 & \textbullet{} First stable release                  \\
  \bottomrule
\end{tabular}

\chapter{Introduction}

\section{Purpose}

This document aims to identify and create a complete set of Software and Hardware Requirements Specifications for implementing the SIDLOC system.
The analysis shall contain an architectural overview with a high-level design and requirements specification, which will be the basis to bootstrap the development and integration process.
Such an analysis is necessary to provide the implementers with a clear view and understanding of the system under development as well as define standard interfaces for users of the system.

\section{Conventions}

The key words ``MUST'', ``MUST NOT'', ``REQUIRED'', ``SHALL'', ``SHALL NOT'', ``SHOULD'', ``SHOULD NOT'', ``RECOMMENDED'', ``MAY'', and ``OPTIONAL'' in this document are to be interpreted as described in RFC2119.
However, for readability, these words do not appear in all upper case letters in this specification.

\section{Intended Audience}

The intended audience of this document is hardware and software developers who will implement SIDLOC.\@
It is assumed that the reader is familiar with Radio Communications, Digital Signal Processing and has knowledge of electronics and software engineering as well as satellite systems integration in general.

\chapter{Description}

\section{System Functions}

SIDLOC is a satellite tracking and identification system which can detect, catalogue and predict the positions of space objects.
The goal of this system is to support and promote \acrshort{ssa} and \acrshort{stm}, which are expected to play a key role in the near future with the ever-expanding demand for satellites and their miniaturization.
The system can provide a cost-effective solution for risk management, both while in orbit and during post-commissioning or re-entry.
It can also serve as an independent source of spacecraft identification and tracking information that can be used to enhance the existing ones or as a backup solution.

\section{System Perspective}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{images/sidloc-functions.pdf}
  \caption{SIDLOC Components}\label{fig:sidloc-functions}
\end{figure}

The SIDLOC relies on \acrshort{dsss}-based RF transmissions to identify and estimate the orbital characteristics of multiple spacecraft uniquely.
To achieve this, SIDLOC actually consists of three different components.
The specification itself mandates the RF transmission characteristics, the hardware beacon, which is responsible for producing the RF transmissions while in orbit, and the ground station, which is enforced with the demodulation of the SIDLOC RF transmissions and the extraction of the identification and tracking information.

On the space segment, the SIDLOC Beacon is an autonomous communication board that requires minimal integration effort.
The beacon consists of a main controller, a \acrshort{dsp} unit, radio and RF-frontend, and a power management system.
The \acrshort{dsp} and radio systems are responsible for the implementation of the \acrshort{dsss}, the spreading of the baseband signal and the modulation of the resulting sequence.
The RF-frontend manages the signal amplification and the filtering, so the transmissions conform with the SFCG21--2R4~\cite{SFCG-21-2R4}.
On board the beacon, there is an autonomous power management system that is responsible for battery charging through solar panels.
The beacon also provides a basic connectivity interface with the carrier spacecraft and an option for an external power source.

On the ground segment, each \acrshort{gs} continuously receives RF signals and tries to identify SIDLOC transmissions.
For each identified SIDLOC transmission, the \acrshort{gs} also estimates its orbit.
This is accomplished using \acrshort{dsp} techniques by extracting the frequency offset of the received \acrshort{dsss} signal that is introduced due to the Doppler effect.
The resulting information can then be forwarded to an open-access database for further analysis, visualisation and storage.


\section{Operating Environment}

The SIDLOC Beacon hardware operates on \acrshort{leo} in an autonomous and unattended way.
The beacon is intended to be mounted on a carrier spacecraft, and it does not stand as a spacecraft, rather than an external component.
It utilizes a dedicated \acrshort{uhf} channel that is shared between other SIDLOC beacons that may transmit concurrently.
Each beacon is assigned a unique identifier that allows the spacecraft to be uniquely identifiable.
The SIDLOC system relies on these identifiers and their proper assignment by a control and coordination authority.

From the ground segment perspective, each \acrshort{gs} may have multiple SIDLOC beacons in its field of view.
Each \acrshort{gs} can process transmissions from a number of different beacons, depending on the processing capabilities of the computing platform.
For better performance, especially in the localisation process, the SIDLOC system assumes a dense network of synchronized \acrshort{gs}.

\section{Design and Implementation Constraints}

The SIDLOC beacon is strictly constrained in size.
The beacon shall be applicable in spacecraft from 12U down to PocketQubes or $1/4$ U satellites.
This poses several limitations regarding the power consumption, heat anticipation, maximum transmission power and the overall complexity of the beacon due to the limited real estate on the \acrshort{pcb}.

In addition, due to the low power constraints and the long duration of the \acrshort{dsss} transmission, the interval between transmissions may allow each \acrshort{gs} to receive a limited number of transmissions for each satellite pass.
To reduce the identification time and the localisation error, the SIDLOC relies on a network of \acrshort{gs}.
Each of the \acrshort{gs}, however, should be accurately synchronized both in time and in frequency.
The easiest way is for each \acrshort{gs} to be assisted by a \acrshort{gpsdo}.

\section{Documentation}

The documentation for the SIDLOC system is available at the Gitlab repository \url{https://gitlab.com/librespacefoundation/sidloc/sidloc-docs}.
This repository contains rendered documents for the SIDLOC system requirements, the specification itself and other technical documents.
Additional information can be found at the SIDLOC website \url{https://sidloc.org/}.

For implementation-specific details, documentation can also be found in the repositories of the corresponding components under the Gitlab group \url{https://gitlab.com/librespacefoundation/sidloc}.

\section{Assumption and Dependencies}

The SIDLOC system assumes the usage of a dedicated frequency shared by all beacons in orbit.
To mitigate the interference issue, SIDLOC uses low transmission power and the \acrshort{dsss} technology.
In order to exploit fully the advantages of the \acrshort{dsss}, each beacon should use a unique spreading polynomial.
Yet, the number of the unique spreading polynomials is finite and while the number of beacons may scale drastically, coordination is assumed, so beacons that share the same spreading polynomials do not interfere with each other.

As mentioned in previous sections, the \acrshort{gs} should be able to perform \acrshort{dsp} either in software or in hardware.
Due to the limited number of SIDLOC transmissions that a \acrshort{gs} may receive per pass, the system assumes a network of connected \acrshort{gs}.
Moreover, to reduce the tracking error, the \acrshort{gs} may be synchronized in frequency and time.

Regarding the SIDLOC Beacon, it is assumed to operate autonomously by harvesting solar power.
For this reason, the beacon should be mounted so that the spacecraft does not obstruct the solar panels.
Furthermore, even if the beacon and the specification design consider the usage of an omnidirectional antenna, the field of view with the earth shall be clear.

\chapter{External Interfaces}

\section{User Interfaces}

The SIDLOC Beacon itself doesn't need any user interface.
However, user interfaces are necessary for any stakeholders that need access to the received RF signals, their metadata, and any data that comes from their analysis, such as spectrograms, identifications data, localisation estimations etc.
These user interfaces will be available and provided by the ground segment infrastructure.

\section{Software Interfaces}

The SIDLOC system shall provide software interfaces for accessing identification and tracking information from every \acrshort{gs} separately or as a network of connected \acrshort{gs}.
Taking into account the experience gained from operating the SatNOGS network, a similar approach with REST APIs will also be utilized for the SIDLOC.\@

\section{Hardware Interfaces}

The purpose of this system is to accompany spacecraft to assist in \acrshort{leop} and spacecraft tracking.
The SIDLOC Beacon is intended to be attached externally to the spacecraft and can operate independently of the spacecraft itself.
The antenna is included in the hardware, as well as an optional electrical interface for power and bus communication, where applicable.

\section{Communication Interfaces}

The beacon hardware uses an RF interface for space-to-ground and space-to-space communications.
Optionally the beacon can communicate with the spacecraft bus to obtain spacecraft health information and/or positional data.
Bus communication is achieved either by a \acrshort{can} or a Serial interface.
These interfaces function in receive-only mode and tap on the bus data stream.

\IfFileExists{include/requirements}{\input{include/requirements}}{} % chktex 27

\chapter{Experiment Requirements}

\section{Functional Requirements}

\ifdefined\ExperimentFunctional{}
  \ExperimentFunctional{}
\fi
\clearpage

\section{Interface Requirements}

\ifdefined\ExperimentInterface{}
  \ExperimentInterface{}
\fi
\clearpage

\section{Operational Requirements}

\ifdefined\ExperimentOperational{}
  \ExperimentOperational{}
\fi
\clearpage

\section{Configuration Requirements}

\ifdefined\ExperimentConfiguration{}
  \ExperimentConfiguration{}
\fi
\clearpage

\section{Design Requirements}

\ifdefined\ExperimentDesign{}
  \ExperimentDesign{}
\fi
\clearpage

\section{Performance Requirements}

\ifdefined\ExperimentPerformance{}
  \ExperimentPerformance{}
\fi
\clearpage

\section{Regulatory Requirements}

\ifdefined\ExperimentRegulatory{}
  \ExperimentRegulatory{}
\fi
\clearpage

\chapter{Specification Requirements}

\section{Functional Requirements}

\ifdefined\SpecificationFunctional{}
  \SpecificationFunctional{}
\fi
\clearpage

\section{Communications Requirements}

\ifdefined\SpecificationCommunications{}
  \SpecificationCommunications{}
\fi
\clearpage

\section{Interface Requirements}

\ifdefined\SpecificationInterface{}
  \SpecificationInterface{}
\fi
\clearpage

\section{Operational Requirements}

\ifdefined\SpecificationOperational{}
  \SpecificationOperational{}
\fi
\clearpage

\section{Design Requirements}

\ifdefined\SpecificationDesign{}
  \SpecificationDesign{}
\fi
\clearpage

\section{Performance Requirements}

\ifdefined\SpecificationPerformance{}
  \SpecificationPerformance{}
\fi
\clearpage

\section{Regulatory Requirements}

\ifdefined\SpecificationRegulatory{}
  \SpecificationRegulatory{}
\fi
\clearpage

\begin{appendices}

  \chapter{Glossary and Acronyms}

  \printglossary[type=\acronymtype]

\end{appendices}

\bibliographystyle{unsrt}
\bibliography{references}

\end{document}
